package com.yourcompany.Tests;

import com.yourcompany.Pages.*;
import org.junit.Test;
import org.openqa.selenium.InvalidElementStateException;

import static org.junit.Assert.*;

public class PostPurchaseTest extends TestBase {

    public PostPurchaseTest(String os,
                          String version, String browser, String deviceName, String deviceOrientation) {
        super(os, version, browser, deviceName, deviceOrientation);
    }

    /**
     * Runs a simple test verifying link can be followed.
     * @throws InvalidElementStateException
     */
    @Test
    public void verifyLinkTest() throws InvalidElementStateException {
        PostPurchasePage page = PostPurchasePage.visitPage(driver);

        String text = page.getText();
        assertEquals("Unforgettable Starts Here", text);
//        page.followLink();
//        page.takeScreenshot(this.getOS(), this.getBrowser());

//        assertFalse(page.isOnPage());
    }
}